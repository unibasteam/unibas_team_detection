FROM registry.gitlab.com/competitions4/sciroc/dockers/sciroc:1.5

LABEL maintainer="Daniel López Puig <daniel.lopez@pal-robotics.com>"

ARG REPO_WS=/ws
RUN mkdir -p $REPO_WS/src
WORKDIR /home/user/$REPO_WS

# Copy my-ros-pkg inside the docker to later be compiled in

# I want my docker image to have git installed
RUN apt install -y git

RUN apt-get update

RUN apt-get install -y python3.8

RUN apt-get install curl

RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

RUN python3.8 get-pip.py

RUN pip install tensorflow==2.6.0 opencv-python mediapipe sklearn matplotlib

RUN apt install python3-pip -y

RUN apt install gedit -y

RUN apt install nano -y

RUN pip3 install appJar

RUN apt-get install python3-tk -y

RUN pip install rospkg

COPY ./ws /home/user/ws/src

# Build and source  my-ros-pkg packages 
RUN bash -c "source /opt/pal/ferrum/setup.bash \
    && catkin build \
    && echo 'source /opt/pal/ferrum/setup.bash' >> ~/.bashrc \
    && echo 'source devel/setup.bash' >> ~/.bashrc "


ENTRYPOINT ["bash"]
